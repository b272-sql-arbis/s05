1.
select customerName from customers where country = "Philippines";

2.
select contactLastName, contactFirstName from customers where customerName = "La Rochelle Gifts";

3.
select productName, MSRP from products where productName = "The Titanic";

4.
select firstName, lastName from employees where email = "jfirrelli@classicmodelcars.com";

5.
select customerName from customers where state is null;

6.
select firstName, lastName, email from employees where lastName = "Patterson" and firstName = "Steve";

7.
select customerName, country, creditLimit from customers where country != "USA" and creditLimit > 3000;

8.
select customerNumber from orders where comments like "%DHL%";

9.
select productLine from products where productDescription like "%state of the art%";

10.
select distinct country from customers;

11.
select distinct status from orders;

12.
select customerName, country from customers where country in ("USA", "France", "Canada");

13.
select firstName, lastName, officeCode from employees where officeCode = 5;

14.
select customerName from customers where salesRepEmployeeNumber = 1166;

15.
select products.productName, customers.customerName
from orders
join customers on orders.customerNumber = customers.customerNumber
join orderdetails on orders.orderNumber = orderdetails.orderNumber
join products on orderdetails.productCode = products.productCode
where orders.customerNumber = 121;

16.
select employees.firstName, employees.lastName, customers.customerName, offices.country
from employees
join offices on employees.officeCode = offices.officeCode
join customers on customers.country = offices.country;

17.
select productName, quantityInStock from products where productLine = "Planes";

18.
select customerName from customers where phone like "%+81%";